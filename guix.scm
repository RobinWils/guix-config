                                                                            Documents/guix                                                                                                 

;; This is an operating system configuration template
;; for a "desktop" setup with the xmonad window manager.

(use-modules (gnu) (gnu system nss))
(use-service-modules desktop)
(use-package-modules haskell ssh certs wm)

(operating-system
  (host-name "[Guix]ian-the-[S]pooky-[D]ark-wizard")
  (timezone "Europe/Brussels")
  (locale "en_US.utf8")

  ;; Use GRUB bootloader
  ;; Partition mounted on /dev/sda1
  (bootloader (bootloader-configuration
                (bootloader grub-bootloader)
                (target "/dev/sda")))

  ;; Assume the target root file system is labelled "root"
  (file-systems (cons (file-system
                         (device "root")
                         (mount-point "/")
                         (type "ext4"))
                       %base-file-systems))

  ;; Create user account
  (users (cons (user-account
                (name "guix")
                (comment "'Guix' is pronounced as 'geeks'")
                (group "users")
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video"))
                (home-directory "/home/guix"))
               %base-user-accounts))

  ;; Add the window manager
  ;; We can choose one at the log-in screen with F1.
  (packages (cons* xmobar openssh ghc   ;xmonad and
                   xmonad ghc-network   ;haskell packages
                   ghc-xmonad-contrib
                   nss-certs            ;for HTTPS access
                   %base-packages))

  ;; Use the "desktop" services, which include the X11
  ;; log-in service, networking with NetworkManager, and more.
  (services %desktop-services)

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))

